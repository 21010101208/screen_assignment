import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class second_screen extends StatelessWidget {
  const second_screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: Container(
                color: Colors.deepPurple[700],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(15, 20, 20, 20),
                          child: CircleAvatar(
                            radius: 30,
                            backgroundImage:
                                AssetImage("assets/images/1st.jpg"),
                          ),
                        ),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(
                                  "Darsh Vekariya",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 10, 0, 0),
                                child: Text(
                                  "21010101208@darshan.ac.in",
                                  style: TextStyle(
                                    fontFamily: 'Mono',
                                      fontSize: 14,
                                      color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 7),
                                  child: Text("200",
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Container(
                                  child: Text(
                                    "Favorite Place",
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 7),
                                  child: Text("50",
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Container(
                                  child: Text("Tours ",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white)),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 7),
                                  child: Text("100",
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Container(
                                  child: Text("Photos",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white)),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              flex: 2,
            ),
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(15, 25, 0, 0),
                            child: Text(
                              "Favorites Place",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            height: 475,
                            child: ListView(
                              scrollDirection: Axis.vertical,
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              Container(
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  child: Image.asset(
                                                    "assets/images/germany.jpg",
                                                    fit: BoxFit.fill,
                                                    height: 150,
                                                  ),
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Germany",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              Container(
                                                child: ClipRRect(
                                                  borderRadius:
                                                  BorderRadius.circular(5),
                                                  child: Image.asset(
                                                    "assets/images/dubai.webp",
                                                    fit: BoxFit.fill,
                                                    height: 150,
                                                  ),
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Dubai",
                                                    style: TextStyle(
                                                        fontWeight:
                                                        FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/london.jpg",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("London",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/maldives.jpg",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Maldives",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/parish.jpg",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Parish",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/switzerland.webp",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Switzerland",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/taj.webp",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("India",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/bangkok.jpg",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Bangkok",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/thailand.jpg",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Thailand",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: Image.asset(
                                                  "assets/images/goa.jpg",
                                                  fit: BoxFit.fill,
                                                  height: 150,
                                                ),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(5),
                                                child: Container(
                                                  height: 75,
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Colors.black,
                                                            const Color(0x00AAAAAA),
                                                          ],
                                                          begin: Alignment.bottomLeft,
                                                          end: Alignment.topLeft,
                                                          stops: [0.0, 1.0],
                                                          tileMode: TileMode.clamp
                                                      )
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text("Goa",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                          height: 150,
                                          margin: EdgeInsets.only(right: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              flex: 5,
            ),
          ],
        ),
      ),
    );
  }
}
