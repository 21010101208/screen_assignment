import 'package:assingment/first_page.dart';
import 'package:assingment/second_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';


class NavigatorWidget extends StatelessWidget {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Drawer(
        child: Material(
          child: Stack(
            children: [
              ClipRRect(
                child: Image.asset(
                  "assets/images/forest.jpg",
                  fit: BoxFit.fill,
                  height: double.infinity,
                ),
              ),
              Container(
                  color: Color(0x99AAAAAA),
              ),
              ListView(
                children: <Widget>[
                  Container(child: CircleAvatar(
                    radius: 50,
                    backgroundImage:
                    AssetImage("assets/images/1st.jpg"),
                  ), ),
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                      "Darsh  Vekariya",
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  buildMenuItem(
                    text: 'LogIn Page',
                    icon: Icons.login_outlined,
                    onClicked: () => selectedItem(context, 0),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  buildMenuItem(
                    text: 'Detail Page',
                    icon: Icons.details_rounded,
                    onClicked: () => selectedItem(context, 1),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget buildMenuItem({
  required String text,
  required IconData icon,
  VoidCallback? onClicked,
}) {
  return ListTile(
    leading: Icon(
      icon,
      color: Colors.white,
    ),
    title: Text(
      text,
      style: TextStyle(color: Colors.white),
    ),
    onTap: onClicked,
  );
}

void selectedItem(BuildContext context, int index) {
  Navigator.of(context).pop();
  switch (index) {
    case 0:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => first_scren(),
      ));
      break;
    case 1:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => second_screen(),
      ));
      break;
  }
}
