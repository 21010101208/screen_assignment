import 'package:flutter/material.dart';
import 'Drawer.dart';
import 'package:badges/badges.dart';

class Third_Screen extends StatelessWidget {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: NavigatorWidget(),
        key: scaffoldKey,
        appBar: AppBar(
          elevation: double.maxFinite,
          leading: IconButton(
            icon: Icon(Icons.dehaze_rounded, size: 30), //icon
            onPressed: () {
              if (scaffoldKey.currentState!.isDrawerOpen) {
                scaffoldKey.currentState!.closeDrawer();
              } else {
                scaffoldKey.currentState!.openDrawer();
              }
            },
          ),
          toolbarHeight: 75,
          backgroundColor: Colors.deepPurple[700],
          title: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Text("Home"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            child: Badge(
                              badgeColor: Colors.red,
                              child: Icon(
                                Icons.notifications,
                                size: 25,
                              ),
                              position: BadgePosition.topEnd(top: 0, end: 0),
                            ),
                          ),
                          Container(
                            child: CircleAvatar(
                              radius: 17,
                              backgroundImage:
                                  AssetImage("assets/images/1st.jpg"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            Container(
              color: Colors.deepPurple[700],
              child: Stack(children: [
                Container(
                  color: Colors.deepPurple[700],
                  child: Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Container(
                              height: 25,
                              color: Colors.deepPurple[700],
                            )
                          ],
                        ),
                      ),
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.white)),
                        child: Column(
                          children: [
                            Container(
                              height: 25,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                  elevation: 10,
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: const TextField(
                        decoration: InputDecoration(
                          hintText: "Serch Your Destinatation",
                          prefixIcon: Icon(Icons.search, color: Colors.black),
                          suffixIcon: Icon(Icons.tune),
                        ),
                      )),
                ),
              ]),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: Column(children: [
                  Container(
                    color: Colors.white,
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            child: Stack(
                              alignment: Alignment.bottomLeft,
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(7),
                                    child: Container(color: Colors.blue)),
                                Center(
                                  child: Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.all(15),
                                    child: Icon(Icons.flight, size: 30),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(15,10,10,10),
                                  child: Text("Flights",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.white)),
                                ),
                              ],
                            ),
                            height: 100,
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(vertical: 20),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: Stack(
                              alignment: Alignment.bottomLeft,
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(7),
                                    child: Container(color: Colors.green)),
                                Container(
                                  alignment: Alignment.topLeft,
                                  margin: EdgeInsets.all(15),
                                  child: Icon(Icons.holiday_village_sharp,
                                      size: 30),
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(15,10,10,10),
                                  child: Text("Hotels",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.white)),
                                ),
                              ],
                            ),
                            height: 100,
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(vertical: 20),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: Stack(
                              alignment: Alignment.bottomLeft,
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(7),
                                    child: Container(color: Colors.redAccent)),
                                Center(
                                  child: Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.all(15),
                                    child: Icon(Icons.explore, size: 30),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(15,10,10,10),
                                  child: Text("Explore",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.white)),
                                ),
                              ],
                            ),
                            height: 100,
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(vertical: 20),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Container(
                        margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Text("Popular Destinations",
                            style: TextStyle(fontFamily: 'Oswald',
                                fontSize: 20, fontWeight: FontWeight.bold))),
                  ),
                  Expanded(
                    child: Container(
                      height: double.infinity,
                      child: ListView(
                        scrollDirection: Axis.vertical,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.asset(
                                            "assets/images/germany.jpg",
                                            fit: BoxFit.cover,
                                            height: 150,
                                            width: double.infinity,
                                          ),
                                        ),
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(5),
                                          child: Container(
                                            height: 100,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Colors.black,
                                                      const Color(0x00AAAAAA),
                                                    ],
                                                    begin: Alignment.bottomLeft,
                                                    end: Alignment.topLeft,
                                                    stops: [0.0, 1.0],
                                                    tileMode: TileMode.clamp
                                                )
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text("Germany",
                                                  style: TextStyle(fontFamily: 'RobotoSlab',
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 25,
                                                      color: Colors.white)),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              child: Text(
                                                  "World's Best Visiting Place",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    height: 150,
                                    margin: EdgeInsets.only(right: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.asset(
                                            "assets/images/parish.jpg",
                                            fit: BoxFit.cover,
                                            height: 150,
                                            width: double.infinity,
                                          ),
                                        ),
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(5),
                                          child: Container(
                                            height: 100,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Colors.black,
                                                      const Color(0x00AAAAAA),
                                                    ],
                                                    begin: Alignment.bottomLeft,
                                                    end: Alignment.topLeft,
                                                    stops: [0.0, 1.0],
                                                    tileMode: TileMode.clamp
                                                )
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text("Parish",
                                                  style: TextStyle(fontFamily: 'RobotoSlab',
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 25,
                                                      color: Colors.white)),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              child: Text(
                                                  "World's Best Visiting Place",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    height: 150,
                                    margin: EdgeInsets.only(right: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.asset(
                                            "assets/images/london.jpg",
                                            fit: BoxFit.cover,
                                            height: 150,
                                            width: double.infinity,
                                          ),
                                        ),
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(5),
                                          child: Container(
                                            height: 100,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Colors.black,
                                                      const Color(0x00AAAAAA),
                                                    ],
                                                    begin: Alignment.bottomLeft,
                                                    end: Alignment.topLeft,
                                                    stops: [0.0, 1.0],
                                                    tileMode: TileMode.clamp
                                                )
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text("London",
                                                  style: TextStyle(fontFamily: 'RobotoSlab',
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 25,
                                                      color: Colors.white)),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              child: Text(
                                                  "World's Best Visiting Place",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    height: 150,
                                    margin: EdgeInsets.only(right: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.asset(
                                            "assets/images/goa.jpg",
                                            fit: BoxFit.cover,
                                            height: 150,
                                            width: double.infinity,
                                          ),
                                        ),
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Container(
                                            height: 100,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Colors.black,
                                                      const Color(0x00AAAAAA),
                                                    ],
                                                    begin: Alignment.bottomLeft,
                                                    end: Alignment.topLeft,
                                                    stops: [0.0, 1.0],
                                                    tileMode: TileMode.clamp
                                                )
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text("Goa",
                                                  style: TextStyle(fontFamily: 'RobotoSlab',
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 25,
                                                      color: Colors.white)),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              child: Text(
                                                  "World's Best Visiting Place",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    height: 150,
                                    margin: EdgeInsets.only(right: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.asset(
                                            "assets/images/dubai.webp",
                                            fit: BoxFit.cover,
                                            height: 150,
                                            width: double.infinity,
                                          ),
                                        ),
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(5),
                                          child: Container(
                                            height: 100,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Colors.black,
                                                      const Color(0x00AAAAAA),
                                                    ],
                                                    begin: Alignment.bottomLeft,
                                                    end: Alignment.topLeft,
                                                    stops: [0.0, 1.0],
                                                    tileMode: TileMode.clamp
                                                )
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text("Dubai",
                                                  style: TextStyle(fontFamily: 'RobotoSlab',
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 25,
                                                      color: Colors.white)),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              child: Text(
                                                  "World's Best Visiting Place",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    height: 150,
                                    margin: EdgeInsets.only(right: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Stack(
                                      alignment: Alignment.bottomLeft,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.asset(
                                            "assets/images/bangkok.jpg",
                                            fit: BoxFit.cover,
                                            height: 150,
                                            width: double.infinity,
                                          ),
                                        ),
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(5),
                                          child: Container(
                                            height: 100,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Colors.black,
                                                      const Color(0x00AAAAAA),
                                                    ],
                                                    begin: Alignment.bottomLeft,
                                                    end: Alignment.topLeft,
                                                    stops: [0.0, 1.0],
                                                    tileMode: TileMode.clamp
                                                )
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text("Bangkok",
                                                  style: TextStyle(fontFamily: 'RobotoSlab',
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 25,
                                                      color: Colors.white)),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              child: Text(
                                                  "World's Best Visiting Place",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    height: 150,
                                    margin: EdgeInsets.only(right: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
              ),
              flex: 5,
            ),
          ],
        ),
      ),
    );
  }
}
