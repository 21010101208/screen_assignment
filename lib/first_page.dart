import 'package:assingment/third_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_login_buttons/social_login_buttons.dart';

class first_scren extends StatelessWidget {
  const first_scren({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Stack(
                children: [
                  Container(
                    child: Image.asset(
                      "assets/images/1st.jpg",
                      fit: BoxFit.cover,
                    ),
                    width: double.infinity,
                    height: double.infinity,
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(40),
                          topLeft: Radius.circular(40),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Text(
                              "Enjoy  The  World \n With  Us",
                              style: TextStyle(
                                  color: Colors.deepPurple[700],
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(40),
                            child: SocialLoginButton(
                              backgroundColor: Colors.deepPurple[700],
                              height: 50,
                              width: 200,
                              text: 'LOG IN',
                              borderRadius: 5,
                              fontSize: 20,
                              buttonType: SocialLoginButtonType.generalLogin,
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Third_Screen()),
                                  );
                                }
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 30),
                            child: Text(
                              "Create an account",
                              style: TextStyle(
                                color: Colors.indigo,
                                fontSize: 20,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
